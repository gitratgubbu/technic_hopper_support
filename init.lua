if minetest.get_modpath("hopper") and minetest.get_modpath("technic") then
	-- Add support for the hopper mod using its api https://github.com/minetest-mods/hopper/blob/master/api.lua
	hopper:add_container({
		-- hv generator
		{ "bottom", "technic:hv_generator", "src" },
		{ "side", "technic:hv_generator", "src" },
		{ "bottom", "technic:hv_generator_active", "src" },
		{ "side", "technic:hv_generator_active", "src" },
		-- mv_centrifuge
		{ "top", "technic:mv_centrifuge", "dst" },
		{ "top", "technic:mv_centrifuge_active", "dst" },
		{ "bottom", "technic:mv_centrifuge", "src" },
		{ "bottom", "technic:mv_centrifuge_active", "src" },
		{ "side", "technic:mv_centrifuge", "src" },
		{ "side", "technic:mv_centrifuge_active", "src" },
		-- mv_freezer
		{ "top", "technic:mv_freezer", "dst" },
		{ "top", "technic:mv_freezer_active", "dst" },
		{ "bottom", "technic:mv_freezer", "src" },
		{ "bottom", "technic:mv_freezer_active", "src" },
		{ "side", "technic:mv_freezer", "src" },
		{ "side", "technic:mv_freezer_active", "src" },

		---
		--- MV Machines that also are LV mashines
		---
		-- mv extractor
		{ "top", "technic:mv_extractor", "dst" },
		{ "top", "technic:mv_extractor_active", "dst" },
		{ "bottom", "technic:mv_extractor", "src" },
		{ "bottom", "technic:mv_extractor_active", "src" },
		{ "side", "technic:mv_extractor", "src" },
		{ "side", "technic:mv_extractor_active", "src" },
		-- mv_electric_furnace
		{ "top", "technic:mv_electric_furnace", "dst" },
		{ "top", "technic:mv_electric_furnace_active", "dst" },
		{ "bottom", "technic:mv_electric_furnace", "src" },
		{ "bottom", "technic:mv_electric_furnace_active", "src" },
		{ "side", "technic:mv_electric_furnace", "src" },
		{ "side", "technic:mv_electric_furnace_active", "src" },
		-- mv_compressor
		{ "top", "technic:mv_compressor", "dst" },
		{ "top", "technic:mv_compressor_active", "dst" },
		{ "bottom", "technic:mv_compressor", "src" },
		{ "bottom", "technic:mv_compressor_active", "src" },
		{ "side", "technic:mv_compressor", "src" },
		{ "side", "technic:mv_compressor_active", "src" },
		-- mv_grinder
		{ "top", "technic:mv_grinder", "dst" },
		{ "top", "technic:mv_grinder_active", "dst" },
		{ "bottom", "technic:mv_grinder", "src" },
		{ "bottom", "technic:mv_grinder_active", "src" },
		{ "side", "technic:mv_grinder", "src" },
		{ "side", "technic:mv_grinder_active", "src" },
		-- mv generator
		{ "bottom", "technic:mv_generator", "src" },
		{ "side", "technic:mv_generator", "src" },
		{ "bottom", "technic:mv_generator_active", "src" },
		{ "side", "technic:mv_generator_active", "src" },
		-- mv_alloy_furnace
		{ "top", "technic:mv_alloy_furnace", "dst" },
		{ "top", "technic:mv_alloy_furnace_active", "dst" },
		{ "bottom", "technic:mv_alloy_furnace", "src" },
		{ "bottom", "technic:mv_alloy_furnace_active", "src" },
		{ "side", "technic:mv_alloy_furnace", "src" },
		{ "side", "technic:mv_alloy_furnace_active", "src" },
		-- ###### replacing mv with lv ...
		-- lv extractor
		{ "top", "technic:lv_extractor", "dst" },
		{ "top", "technic:lv_extractor_active", "dst" },
		{ "bottom", "technic:lv_extractor", "src" },
		{ "bottom", "technic:lv_extractor_active", "src" },
		{ "side", "technic:lv_extractor", "src" },
		{ "side", "technic:lv_extractor_active", "src" },
		-- lv_electric_furnace
		{ "top", "technic:lv_electric_furnace", "dst" },
		{ "top", "technic:lv_electric_furnace_active", "dst" },
		{ "bottom", "technic:lv_electric_furnace", "src" },
		{ "bottom", "technic:lv_electric_furnace_active", "src" },
		{ "side", "technic:lv_electric_furnace", "src" },
		{ "side", "technic:lv_electric_furnace_active", "src" },
		-- lv_compressor
		{ "top", "technic:lv_compressor", "dst" },
		{ "top", "technic:lv_compressor_active", "dst" },
		{ "bottom", "technic:lv_compressor", "src" },
		{ "bottom", "technic:lv_compressor_active", "src" },
		{ "side", "technic:lv_compressor", "src" },
		{ "side", "technic:lv_compressor_active", "src" },
		-- lv_grinder
		{ "top", "technic:lv_grinder", "dst" },
		{ "top", "technic:lv_grinder_active", "dst" },
		{ "bottom", "technic:lv_grinder", "src" },
		{ "bottom", "technic:lv_grinder_active", "src" },
		{ "side", "technic:lv_grinder", "src" },
		{ "side", "technic:lv_grinder_active", "src" },
		-- lv generator
		{ "bottom", "technic:lv_generator", "src" },
		{ "side", "technic:lv_generator", "src" },
		{ "bottom", "technic:lv_generator_active", "src" },
		{ "side", "technic:lv_generator_active", "src" },
		-- lv_alloy_furnace
		{ "top", "technic:lv_alloy_furnace", "dst" },
		{ "top", "technic:lv_alloy_furnace_active", "dst" },
		{ "bottom", "technic:lv_alloy_furnace", "src" },
		{ "bottom", "technic:lv_alloy_furnace_active", "src" },
		{ "side", "technic:lv_alloy_furnace", "src" },
		{ "side", "technic:lv_alloy_furnace_active", "src" },
		-- #######
		-- quarry
		-- {"top", "technic:quarry", "cache"}, -- this does not work: Manually taking / removing from cache by hand is not possible if you can not wait, restart or disable the quarry to start automatic purge
		-- tool_workshop
		{ "bottom", "technic:tool_workshop", "src" },
		{ "side", "technic:tool_workshop", "src" },
		{ "bottom", "technic:tool_workshop_active", "src" },
		{ "side", "technic:tool_workshop_active", "src" },
		-- chests support
		{ "top", "technic:copper_chest", "main" },
		{ "side", "technic:copper_chest", "main" },
		{ "bottom", "technic:copper_chest", "main" },
		{ "top", "technic:iron_chest", "main" },
		{ "side", "technic:iron_chest", "main" },
		{ "bottom", "technic:iron_chest", "main" },
		{ "top", "technic:gold_chest", "main" },
		{ "side", "technic:gold_chest", "main" },
		{ "bottom", "technic:gold_chest", "main" },
		{ "top", "technic:silver_chest", "main" },
		{ "side", "technic:silver_chest", "main" },
		{ "bottom", "technic:silver_chest", "main" },
		-- Battery Box support, they have 8 charging stages ...for now only support charging of batteries in src slot
		-- mv battery box
		{ "top", "technic:mv_battery_box8", "src" },
		{ "side", "technic:mv_battery_box8", "src" },
		{ "bottom", "technic:mv_battery_box8", "src" },
		-- {"top", "technic:mv_battery_box0", "src"}, -- imo it does not make sense to take a propably empty battery from an empty battery battery box ...
		{ "side", "technic:mv_battery_box0", "src" },
		{ "bottom", "technic:mv_battery_box0", "src" },
		{ "top", "technic:mv_battery_box1", "src" },
		{ "side", "technic:mv_battery_box1", "src" },
		{ "bottom", "technic:mv_battery_box1", "src" },
		{ "top", "technic:mv_battery_box2", "src" },
		{ "side", "technic:mv_battery_box2", "src" },
		{ "bottom", "technic:mv_battery_box2", "src" },
		{ "top", "technic:mv_battery_box3", "src" },
		{ "side", "technic:mv_battery_box3", "src" },
		{ "bottom", "technic:mv_battery_box3", "src" },

		{ "top", "technic:mv_battery_box4", "src" },
		{ "side", "technic:mv_battery_box4", "src" },
		{ "bottom", "technic:mv_battery_box4", "src" },

		{ "top", "technic:mv_battery_box5", "src" },
		{ "side", "technic:mv_battery_box5", "src" },
		{ "bottom", "technic:mv_battery_box5", "src" },

		{ "top", "technic:mv_battery_box6", "src" },
		{ "side", "technic:mv_battery_box6", "src" },
		{ "bottom", "technic:mv_battery_box6", "src" },

		{ "top", "technic:mv_battery_box7", "src" },
		{ "side", "technic:mv_battery_box7", "src" },
		{ "bottom", "technic:mv_battery_box7", "src" },
		-- HV Bat Box, level 8 to 0
		{ "top", "technic:hv_battery_box8", "src" },
		{ "side", "technic:hv_battery_box8", "src" },
		{ "bottom", "technic:hv_battery_box8", "src" },
		-- {"top", "technic:hv_battery_box0", "src"}, -- imo it does not make sense to take a propably empty battery from an empty battery battery box ...
		{ "side", "technic:hv_battery_box0", "src" },
		{ "bottom", "technic:hv_battery_box0", "src" },
		{ "top", "technic:hv_battery_box1", "src" },
		{ "side", "technic:hv_battery_box1", "src" },
		{ "bottom", "technic:hv_battery_box1", "src" },
		{ "top", "technic:hv_battery_box2", "src" },
		{ "side", "technic:hv_battery_box2", "src" },
		{ "bottom", "technic:hv_battery_box2", "src" },
		{ "top", "technic:hv_battery_box3", "src" },
		{ "side", "technic:hv_battery_box3", "src" },
		{ "bottom", "technic:hv_battery_box3", "src" },

		{ "top", "technic:hv_battery_box4", "src" },
		{ "side", "technic:hv_battery_box4", "src" },
		{ "bottom", "technic:hv_battery_box4", "src" },

		{ "top", "technic:hv_battery_box5", "src" },
		{ "side", "technic:hv_battery_box5", "src" },
		{ "bottom", "technic:hv_battery_box5", "src" },

		{ "top", "technic:hv_battery_box6", "src" },
		{ "side", "technic:hv_battery_box6", "src" },
		{ "bottom", "technic:hv_battery_box6", "src" },

		{ "top", "technic:hv_battery_box7", "src" },
		{ "side", "technic:hv_battery_box7", "src" },
		{ "bottom", "technic:hv_battery_box7", "src" },
		-- LV Battery Box
		{ "top", "technic:lv_battery_box8", "src" },
		{ "side", "technic:lv_battery_box8", "src" },
		{ "bottom", "technic:lv_battery_box8", "src" },
		-- {"top", "technic:lv_battery_box0", "src"}, -- imo it does not make sense to take a propably empty battery from an empty battery battery box ...
		{ "side", "technic:lv_battery_box0", "src" },
		{ "bottom", "technic:lv_battery_box0", "src" },
		{ "top", "technic:lv_battery_box1", "src" },
		{ "side", "technic:lv_battery_box1", "src" },
		{ "bottom", "technic:lv_battery_box1", "src" },
		{ "top", "technic:lv_battery_box2", "src" },
		{ "side", "technic:lv_battery_box2", "src" },
		{ "bottom", "technic:lv_battery_box2", "src" },
		{ "top", "technic:lv_battery_box3", "src" },
		{ "side", "technic:lv_battery_box3", "src" },
		{ "bottom", "technic:lv_battery_box3", "src" },

		{ "top", "technic:lv_battery_box4", "src" },
		{ "side", "technic:lv_battery_box4", "src" },
		{ "bottom", "technic:lv_battery_box4", "src" },

		{ "top", "technic:lv_battery_box5", "src" },
		{ "side", "technic:lv_battery_box5", "src" },
		{ "bottom", "technic:lv_battery_box5", "src" },

		{ "top", "technic:lv_battery_box6", "src" },
		{ "side", "technic:lv_battery_box6", "src" },
		{ "bottom", "technic:lv_battery_box6", "src" },

		{ "top", "technic:lv_battery_box7", "src" },
		{ "side", "technic:lv_battery_box7", "src" },
		{ "bottom", "technic:lv_battery_box7", "src" },

	})
end
