
# Technic Hopper support

## With this mod Hoppers can ...
- extract finished product (items) from electric furnaces, compressors, alloy furnaces and ... all the other technic machines
- Hoppers can put ores to be smelted into electric furnaces, ... raw materials into all the other technic machines.
    - For example: Hoppers can put fuel into generators
    - Take items from technics copper chests

## Use:

- This Mod has two hard dependencies: hoppers and technic. Without these mods installed this mod is useless

## TODO:
- add support for nuclear power plants (?)

## License:
- MIT

### Repo:
- https://codeberg.org/gitratgubbu/technic_hopper_support

### Credits:
- Gubbu
